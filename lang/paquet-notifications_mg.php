<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-notifications?lang_cible=mg
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// N
	'notifications_description' => 'Active les notifications par email de certaines actions, notamment des forums et de la messagerie de l’espace privé, permet également l’envoi d’un mail aux auteurs lors de la publication de leurs articles.',
	'notifications_slogan' => 'Signaler par mail des modifications'
);
